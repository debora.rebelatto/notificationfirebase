const admin = require("firebase-admin");

var serviceAccount = require("./t2mkt-1bcb1-firebase-adminsdk-qul65-c0780b901d.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://t2mkt-1bcb1.firebaseio.com"
});

module.exports.admin = admin;