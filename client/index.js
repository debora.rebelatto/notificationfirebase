const express = require('express');
const bodyparser = require('body-parser');
const admin = require('./firebase-config')

const app = express();
app.use(bodyparser.json());

app.post('/firebase/notification', (req, res) => {
  /*{
    "notification": { "title": "Portugal vs. Denmark", "body": "great match!" },
    "topic": 'TopSystem'
  }*/
  admin.admin.messaging().send(req.body)
    .then(() => res.status(200).send("Notification sent successfully"));
})

app.listen(3000, () => { console.log("listening to port 3000") })
